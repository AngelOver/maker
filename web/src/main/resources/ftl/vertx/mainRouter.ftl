package ${projectPackage}.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.common.template.TemplateEngine;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import ${projectPackage}.util.DbUtil;
import ${projectPackage}.util.VertxUtil;

/**
* @author: ${Author}
* @date: ${DATE}
*/
public class MainHttpRouter extends AbstractVerticle{

    private TemplateEngine engine;

    public void start() {
        HttpServer server = vertx.createHttpServer();

        DbUtil.getJdbcClient(vertx);  // 初始化连接池

        Router router = Router.router(vertx);
        engine = VertxUtil.getTemplateEngine(vertx);

        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
        router.route("/resource/*").handler(StaticHandler.create("resource").setCachingEnabled(true));
        <#--router.route().handler(CorsHandler.create("vertx\\.io"));-->
        router.route().handler(LoggerHandler.create());

        router.route("/").handler(ctx->{
            engine.render(ctx.data(), "/template/main.ftl", res->{
                ctx.response().putHeader("content-type", "text/html;charset=UTF-8");
                if (res.succeeded()) {
                    ctx.response().end(res.result());
                } else {
                    ctx.fail(res.cause());
                }
            });
        });

        VertxUtil.setRouter(router);

        server.requestHandler(router::accept).listen(8080);
    }
}
