package ldh.maker;

import ldh.maker.component.VertxContentUiFactory;
import ldh.maker.util.UiUtil;

/**
 * Created by ldh123 on 2018/6/23.
 */
public class VertxMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new VertxContentUiFactory());
        UiUtil.setType("vertx");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成vertx + synch 代码";
    }

    public static void main(String[] args) throws Exception {
        throw new RuntimeException("请运行MainLauncher");
//        startDb(null);
//        launch(args);
    }
}
