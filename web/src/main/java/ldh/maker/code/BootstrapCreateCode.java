package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh on 2017/4/6.
 */
public class BootstrapCreateCode extends WebCreateCode {

    public BootstrapCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
//        jspFtls.put("/bootstrap/jspList.ftl", "");
        add("/bootstrap/head.ftl", "head.jsp", "jsp", "common");
        add("/bootstrap/left.ftl", "left.jsp", "jsp", "common");
        add("/bootstrap/mainTag.ftl", "main.tag", "tags");
        add("/bootstrap/jspMain.ftl", "main.jsp", "jsp");
        add("/bootstrap/login.ftl", "login.jsp", "jsp");
        addData("controllerFtl", "/bootstrap/controller.ftl");
//        jsFtls.add("/bootstrap/jsList.ftl");
    }

    @Override
    protected void createOther(){
        super.createOther();
        if (table.isMiddle()) return;
        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        String t = FreeMakerUtil.firstLower(table.getJavaName());
        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createJspPath("jsp", FreeMakerUtil.javaName(table.getName())))
                .ftl("/bootstrap/jspList.ftl")
                .fileName(t + "List.jsp")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createJspPath("jsp", FreeMakerUtil.javaName(table.getName())))
                .ftl("/bootstrap/jspView.ftl")
                .fileName(t + "View.jsp")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createJspPath("jsp", FreeMakerUtil.javaName(table.getName())))
                .ftl("/bootstrap/jspEdit.ftl")
                .fileName(t + "Edit.jsp")
                .make();


    }

    @Override
    protected void createOnce() {
        super.createOnce();

        if (data.getSettingJson().isShiro()) {
            makerShiro();
        }

        String controllerPath = createPath(data.getControllerPackageProperty());
        new MainControllerMaker()
                .controllerPackage(data.getControllerPackageProperty())
                .author(data.getAuthorProperty())
                .ftl("bootstrap/mainController.ftl")
                .fileName("MainController.java")
                .outPath(controllerPath)
                .make();

        new SimpleJavaMaker()
                .auhtor(data.getAuthorProperty())
                .ftl("/bootstrap/LoginController.ftl")
                .fileName("LoginController.java")
                .data("package", data.getControllerPackageProperty())
                .outPath(createPath(data.getControllerPackageProperty()))
                .make();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "webapp", "resource"));
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "webapp", "WEB-INF"));
        try {
            copyResources("common/js", dirs, "common", "js");
            copyResources("common/bootstrap", dirs, "common");
            copyResources("frame", dirs, "frame");
            copyResources("tags", dirst, "tags");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void makerShiro() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPath(projectRootPackage + ".configuration");
        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/bootstrap/AuthRealm.ftl")
                .fileName("AuthRealm.java")
                .outPath(resourcePath)
                .make();

        new SimpleJavaMaker()
                .auhtor(data.getAuthorProperty())
                .projectRootPackage(projectRootPackage)
                .ftl("/bootstrap/ShiroConfiguration.ftl")
                .fileName("ShiroConfiguration.java")
                .outPath(resourcePath)
                .make();
    }

    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .ftl("/bootstrap/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    protected void buildApplicationBootMaker() {
        String projectRootPackage = data.getBasePackageProperty();
        String resourcePath = createPath(projectRootPackage);
//        if (file.exists()) return;
        new ApplicationBootMaker()
                .projectRootPackage(projectRootPackage)
                .author(data.getAuthorProperty())
                .ftl("bootstrap/applicationBoot.ftl")
                .outPath(resourcePath)
                .make();
    }

    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
