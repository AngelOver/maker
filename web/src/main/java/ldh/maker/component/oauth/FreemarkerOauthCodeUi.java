package ldh.maker.component.oauth;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.code.FreemarkerCreateCode;
import ldh.maker.code.FreemarkerOauthCreateCode;
import ldh.maker.component.CodeUi;
import ldh.maker.component.ColumnUi;
import ldh.maker.component.FreemarkerColumnUi;
import ldh.maker.db.SettingDb;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.sql.SQLException;

public class FreemarkerOauthCodeUi extends CodeUi {

    public FreemarkerOauthCodeUi(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        super(treeItem, dbName, tableName);
    }

    @Override
    protected ColumnUi createColumnPane(TreeItem<TreeNode> treeItem, String dbName, String tableName) {
        return new FreemarkerOauthColumnUi(treeItem, dbName, tableName, this);
    }

    @Override
    protected CreateCode buildCreateCode(Table table) throws SQLException {
        SettingData data = SettingDb.loadData(treeItem.getValue().getParent(), dbName);
        CreateCode createCode = new FreemarkerOauthCreateCode(data, treeItem, dbName, table);
        return createCode;
    }
}
