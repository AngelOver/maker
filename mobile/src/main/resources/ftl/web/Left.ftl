<template>
<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
  <#list tableInfo.tables?keys as key>
      <#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
      <li>
          <a href="/#/${util.firstLower(tableInfo.tables[key].javaName)}/list">
            <span data-feather="home"></span>
            ${util.comment(tableInfo.tables[key])}管理
          </a>
      </li>
      </#if>
  </#list>
  </ul>
</div>
</template>


